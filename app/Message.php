<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['body'];

    protected $appends = ['ownMessage'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getOwnMessageAttribute()
    {
        return $this->user_id === auth()->user()->id;
    }
}
